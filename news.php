<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(6)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">


   <div id="toc">
		<div class="container pt20-sm pt10-xs">

		
		<section class="sec-01 wow fadeIn" data-wow-delay="0.5s">
			<div class="head-title center-xs">
				<h2 class="h-text">ข่าวสารและความรู้เกี่ยวกับ Covid-19</h2>
			</div>

			<div class="thm-news-list row _chd-cl-xs-12-xsh-06-sm-03">
				<article class="big _self-cl-xs-12-xsh-12-sm-06">
					<a class="in" href="detail.php" title="ใช้สบู่ หรือเจลแอลกอฮอล์ ลดการติดโควิด-19">
						<figure>
							<img src="di/big-hl1.png" alt="ใช้สบู่ หรือเจลแอลกอฮอล์ ลดการติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>ใช้สบู่ หรือเจลแอลกอฮอล์ ลดการติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>ไขข้อข้องใจอีกครั้ง ใช้ "สบู่" หรือ "เจลแอลกอฮอล์" ล้างมือ จะช่วยลดการติด "โควิด-19" พบเลือกใช้ได้ทั้ง 2 แบบ แต่ต้องดูความสกปรกของมือเป็นหลัก</p>
							
						</div>
					</a>
				</article>
				<article class="big _self-cl-xs-12-xsh-12-sm-06">
					<a class="in" href="detail.php" title="ใช้สบู่ หรือเจลแอลกอฮอล์ ลดการติดโควิด-19">
						<figure>
							<img src="di/big-hl2.png" alt="ใช้สบู่ หรือเจลแอลกอฮอล์ ลดการติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>‘แอปฯ โควิด-19’ แจ้งเตือนคนเสี่ยงติดเชื้อ</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>ไขข้อข้องใจอีกครั้ง ใช้ "สบู่" หรือ "เจลแอลกอฮอล์" ล้างมือ จะช่วยลดการติด "โควิด-19" พบเลือกใช้ได้ทั้ง 2 แบบ แต่ต้องดูความสกปรกของมือเป็นหลัก</p>
							
						</div>
					</a>
				</article>
				
				<? for($i=1;$i<=2;$i++){ ?>
				<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail1.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail2.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail3.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail4.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				
				
				<? } ?>
			</div>
			
			<!--<div class="_flex center-xs mt20-xs">
			<div class="pagination _self-mt0">
                            <ul class="_flex middle-xs center-xs flex-nowrap">
                              <li>
                                <a title="Previous" href="#" class="btn"><i class="fas fa-angle-left"></i> กลับ</a>
                              </li>
                              <li>
                                <a title="Previous" href="#" class="active">1</a>
                              </li>
                              <li>
                                <a title="2" href="#">2</a>
                              </li>
                              <li>
                                <a title="3" href="#">3</a>
                              </li>
                              <li>
                                <a title="4" href="#">4</a>
                              </li>
							  <li>...</li>
							  <li>
                                <a title="20" href="#">20</a>
                              </li>
                              <li>
                                <a title="Next" href="#" class="btn">ถัดไป <i class="fas fa-angle-right"></i></a>
                              </li>
							  <?php /*?><li>
								  <span class="cv-select">
									<select class="select-box bg-wh" id="type-page">
										<option value="0" selected="">10 / page</option>
										<option value="1">20 / page</option>
										<option value="2">30 / page</option>
									</select>
								</span>
							  </li><?php */?>
                            </ul>
                        </div>
		</div>-->
		
			<div class="ctrl-btn d-flex center-xs mt20-xs mb20-xs">
				<a class="ui-btn-border btn-more" href="#" title="Load more">ดูเพิ่ม <i class="fa-angle-down"></i></a>
			</div>
		</section>
		
		
		
		
		

		</div>
  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php /*?><?php include("incs/lightbox.html") ?><?php */?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
