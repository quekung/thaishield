<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(2)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-home">


   <div id="toc">
		<div class="container">

		<div class="full-width bg-hl">
		<section class="sec-hl pt0 container">
			<div class="head-title center-xs">
				<h2 class="h-text wow bounceInDown" data-wow-delay="0.3s">ผู้ป่วยโควิด-19 ที่พบในไทย <small>21 พ.ย. 63 - 4 ธ.ค. 63</small></h2>
			</div>
			<div class="navbr-cv">
				<a class="nav-l wow bounceInLeft" data-wow-delay="0.5s" href="#" title="ข้อมูลประจำวัน"><i class="fa-angle-left"></i> ข้อมูลประจำวัน</a>
				<a class="nav-r wow bounceInRight" data-wow-delay="0.5s" href="#" title="ข้อมูลรวมทั้งหมด">ข้อมูลรวมทั้งหมด <i class="fa-angle-right"></i> </a>
			</div>
			
			<div class="hl-top row mt30-xs">
					<div class="_self-cl-xs-12-sm-06">
						<div class="box sqh1-box rlt w-100 wow fadeIn" data-wow-delay="0.3s">
							<div class="top">
								<h3>มาจากต่างประเทศ<br>เข้า SQ กักกันตัว 14 วัน <big class="t-orange wow bounceIn" data-wow-delay="0.5s">139</big></h3>
							</div>
							<ul class="list-sq">
								<li class=" wow fadeInLeft" data-wow-delay="0.65s"><i class="ic-fight"></i> <div>ทางเครื่องบิน <em class="t-blue2">120</em></div></li>
								<li class=" wow fadeInLeft" data-wow-delay="0.75s"><i class="ic-bus"></i> <div>ทางรถ <em class="t-violet">10</em></div> </li>
								<li class=" wow fadeInLeft" data-wow-delay="0.85s"><i class="ic-boat"></i> <div>ทางเรือ <em class="t-yellow">9</em></div> </li>

							</ul>
						</div>
					</div>
					<div class="_self-cl-xs-12-sm-03">
						<div class="box sqh2-box rlt w-100 wow fadeIn" data-wow-delay="0.35s">
							<div class="top">
								<h3>ลักลอบเข้าผ่าน<br>เส้นทางธรรมชาติ <big class="t-orange wow bounceIn" data-wow-delay="0.6s">15</big></h3>
							</div>
							<ul class="list-sq">

								<li class=" wow fadeInUp" data-wow-delay="0.55s"><div>เชียงใหม่</div> <em>5</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.6s"><div>เชียงราย</div> <em>5</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.65s"><div>พิจิตร</div> <em>2</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.7s"><div>ราชบุรี</div> <em>2</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.75s"><div>กรุงเทพ</div> <em>1</em></li>
							</ul>
						</div>
					</div>
					<div class="_self-cl-xs-12-sm-03">
						<div class="box sqh3-box rlt w-100 wow fadeIn" data-wow-delay="0.4s">
							<div class="top">
								<h3>ติดเชื้อ<br>ภายในประเทศ <big class="t-orange wow bounceIn" data-wow-delay="0.7s">4</big></h3>
							</div>
							<ul class="list-sq">

								<li class=" wow fadeInRight" data-wow-delay="0.65s"><div>ศูนย์พักพิง <br>อ.พบพระ จ.ตาก</div> <em>2</em></li>
								<li class=" wow fadeInRight" data-wow-delay="0.7s"><div>ศูนย์พักพิง <br>อ.พบพระ จ.ตาก</div> <em>1</em></li>
								<li class=" wow fadeIfadeInRightnUp" data-wow-delay="0.75s"><div>ศูนย์พักพิง อ.พบพระ <br>จ.ตาก</div> <em>1</em></li>

							</ul>
						</div>
					</div>
			</div>
					
			
			<div class="hl-bottom mt20-xs">
					
						<div class="box sq1-box rlt w-100 wow bounceInUp" data-wow-delay="0.5s">
							<div class="left">
								<h3>รวมผู้สัมผัส <big class="t-orange">2,590</big></h3>
								<h3>รอตรวจ / ติดตาม <big class="t-orange">120</big></h3>
							</div>
							<ul class="list-sq">

								<li><div>เสี่ยงสูง</div> <em>2,250</em></li>
								<li><div>เสี่ยงต่ำ</div> <em>290</em></li>
								<li><div>อื่นๆ</div> <em>50</em></li>
							</ul>
							
							<ul class="list-news">
								<li>
								<a href="detail.php" title="ผู้สัมผัสต้องปฎิบัติตัวอย่างไร?">
									<figure><img src="di/news-home1.png" alt="ผู้สัมผัสต้องปฎิบัติตัวอย่างไร?"></figure>
									<div class="info">
										<h3>ผู้สัมผัสต้องปฎิบัติตัวอย่างไร?</h3>
										<p>
											<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> 
											<span class="time">07:48 น</span>
										</p>
									</div>
								</a>
								</li>
								<li>
								<a href="detail.php" title="ใครคือผู้สัมผัส ?">
									<figure><img src="di/news-home2.png" alt="ใครคือผู้สัมผัส ?"></figure>
									<div class="info">
										<h3>ใครคือผู้สัมผัส ?</h3>
										<p>
											<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> 
											<span class="time">07:48 น</span>
										</p>
									</div>
								</a>
								</li>
							</ul>
							
							<div class="btn-more mt10-xs txt-r">
							<a href="news.php" title="ติดตามข่าวล่าสุด">ติดตามข่าวล่าสุด <i class="fa-angle-right"></i></a>
							</div>

						</div>


			</div>

		</section>
		</div>
		
		<div class="full-width bg-light">
			<section class="sec-02 container">
				<div class="head-title center-xs wow fadeInDown" data-wow-delay="0.3s">
					<h2 class="h-text">สถานการณ์ COVID-19 ในประเทศไทยรายวัน <small>ข้อมูลล่าสุด 06 ธ.ค. 2563</small></h2>
				</div>
				<div class="row mt30-xs pt30-sm middle-xs">
					<div class="_self-cl-xs-12-sm-06">
						<div class="box th-box rlt wow fadeIn" data-wow-delay="0.3s">
							<div class="top">
								<i class="avatar wow fadeIn" data-wow-delay="0.3s"><img src="di/avatar.png"></i>
								<h3 class="wow bounceInLeft" data-wow-delay="0.3s">ผู้ติดเชื้อสะสม <big class="t-orange">4,039</big></h3>
								<div class="nation">
									<span class="wow bounceInLeft" data-wow-delay="0.4s">คนไทย<em>3,000</em></span>
									<span class="wow bounceInLeft" data-wow-delay="0.45s">ต่างชาติ <em>1,039</em></span>
								</div>
							</div>
							<ul class="d-flex between-xs">
								<li class=" wow fadeInUp" data-wow-delay="0.5s"><div>หายแล้ว</div> <em class="t-green2">3,832</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.6s"><div>อยู่ในโรงพยาบาล</div> <em class="t-blue">139</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.7s"><div>เสียชีวิต</div> <em class="t-red2">60</em></li>
							</ul>

						</div>
					</div>
					<div class="_self-cl-xs-12-sm-06">
						<div class="d-flex center-xs middle-xs">
							<div class="css-donut inner wow fadeInUp" data-wow-delay="0.4s" style="width: 280px; height: 280px">
								<input class="knob" type='text' value='100' data-angleOffset="180" data-angleArc="300" data-fgColor="#64C12B" data-linecap="round" data-thickness=".2" data-width="280" data-height="280" data-displayInput=false />
								<input class="knob" type='text' value='100' data-angleOffset="120" data-angleArc="40" data-fgColor="#50B5FF" data-linecap="round" data-thickness=".2" data-width="280" data-height="280" data-displayInput=false />
								<input class="knob" type='text' value='100' data-angleOffset="160" data-angleArc="20" data-fgColor="#F83E3F" data-linecap="round" data-thickness=".2" data-width="280" data-height="280" data-displayInput=false />
							</div>
							<div class="chart-info">
								<ul class="pl30-sm _chd-mb15 txt-l">
									<li class=" wow bounceInRight" data-wow-delay="0.4s"><i class="point fas fa-circle mr10-sm" style="color: #64C12B"></i> รักษาหาย</li>
									<li class=" wow bounceInRight" data-wow-delay="0.5s"><i class="point fas fa-circle mr10-sm" style="color: #50B5FF"></i> อยู่ในโรงพยาบาล</li>
									<li class=" wow bounceInRight" data-wow-delay="0.6s"><i class="point fas fa-circle mr10-sm" style="color: #F83E3F"></i> เสียชีวิต</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		
		<section class="sec-03">
				<div class="head-title center-xs wow fadeInDown" data-wow-delay="0.3s">
					<h2 class="h-text">แนวโน้มผู้ติดเชื้อ COVID-19 ในประเทศไทย</h2>
				</div>
				<div class="inner z-chart-line wow fadeIn" data-wow-delay="0.3s">
					<!--<img src="di/chart-line.png" alt="demo" style="max-width: 100%">-->
					<canvas id="myChart"></canvas>
					
					<div class="box line-box"  style="top: 50%;left: 50%;transform: translate(-50%,0%)">
						<h3>13 ส.ค. 2563</h3>
						<ul>
							<li><div>ผู้ติดเชื้อ</div> <em class="t-orange">1,345 ราย </em></li>
							<li><div>กำลังรักษา</div> <em class="t-yellow">940 ราย </em></li>
							<li><div>รักษาหาย</div> <em class="t-green2">45 ราย </em></li>
							<li><div>เสียชีวิต</div> <em class="t-red2">1 ราย </em></li>
						</ul>
						<a class="close" href="javascript:;" title="Close" onClick="$(this).parent().fadeOut();"></a>
					</div>
				</div>
				<div class="chart-info">
					<ul class="d-flex start-xs _chd-mr15 wow fadeInUp" data-wow-delay="0.4s">
						<li><i class="point fas fa-circle mr10-sm" style="color: #F99C19"></i> ผู้ติดเชื้อ</li>
						<li><i class="point fas fa-circle mr10-sm" style="color: #FFD914"></i> กำลังรักษา</li>
						<li><i class="point fas fa-circle mr10-sm" style="color: #64C12B"></i> หายแล้ว</li>
						<li><i class="point fas fa-circle mr10-sm" style="color: #F83E3F"></i> เสียชีวิต</li>
					</ul>
				</div>
				
				
				<div class="head-title center-xs mt30-xs pt30-sm wow fadeInDown" data-wow-delay="0.3s">
					<h2 class="h-text">สถานการณ์ State Quarantine ในประเทศไทย<small>ข้อมูลล่าสุด 06 ธ.ค. 2563</small></h2>
				</div>
				<div class="row mt30-xs">
					<div class="_self-cl-xs-12-sm-06">
					
						<div class="box sq1-box rlt wow fadeIn" data-wow-delay="0.3s">
							<div class="left">
								<h3 class=" wow bounceInLeft" data-wow-delay="0.5s">ติดเชื้อภายในประเทศ <big class="t-orange">1,539</big></h3>
								<h3 class=" wow bounceInLeft" data-wow-delay="0.6s">ติดเชื้อจากต่างประเทศ <big class="t-orange">2,000</big></h3>
							</div>
							<ul class="list-sq">
								<li class=" wow fadeInUp" data-wow-delay="0.5s"><div>ก่อนมี (SQ)</div> <em>543</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.55s"><div>เข้า (SQ)</div> <em>2,000</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.6s"><div>ไม่เข้า (SQ)</div> <em>5</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.65s"><div>ส่งกลับต้นทาง</div> <em>10</em></li>
								<li class=" wow fadeInUp" data-wow-delay="0.7s"><div>เส้นทางธรรมชาติ</div> <em>13</em></li>
							</ul>

						</div>
						
						<div class="box sq2-box rlt">
							<ul class="list-sq">
								<li class=" wow bounceInLeft" data-wow-delay="0.5s"><h4>อาสาสมัครสาธารณสุขประจำหมู่บ้าน(อสม.)</h4> <span><em>1,037,45300</em> คน</span></li>
								<li class=" wow bounceInLeft" data-wow-delay="0.6s"><h4>เคาะประตูบ้าน วัดไข้ ให้ความรู้แล้วกว่า</h4> <span><em>25,103,745,399</em> ชม.</span></li>
							</ul>
						</div>
						
					</div>
					<div class="_self-cl-xs-12-sm-06">
						<div class="inner">
						<div class="z-map-th">
							<ul class="hd wow bounceInRight" data-wow-delay="0.5s">
								<li><h3>จำนวน (SQ) ทุกประเภท</h3> <big>543</big> <span>แห่ง</span></li>
								<li><h3>รองรับ</h3> <big>102,453</big> <span>คน</span></li>
							</ul>
							<div class="z-map-world wow fadeIn" data-wow-delay="0.3s">
								<!--<img src="di/map-thailand.png" alt="demo" style="max-width:70%">-->
								<div id="thailand-map" style="height: 680px; width: 100%;"></div>
							</div>
							<div class="box sq-box" style="top: 50%;left: 50%;transform: translate(-50%,0%)">
								<h3>กรุงเทพมหานคร</h3>
								<p class="t-gray2">จำนวนสถานที่ SQ  <em class="t-orange">423</em></p>
								<ul class="d-flex between-xs">
									<li><div>SQ</div> <em>400</em></li>
									<li><div>ASQ</div> <em>13</em></li>
									<li><div>อื่นๆ</div> <em>10</em></li>
								</ul>
								<a class="close" href="javascript:;" title="Close" onClick="$(this).parent().fadeOut();"></a>
							</div>
						</div>
						</div>
					</div>
				</div>
				
				
			</section>
		
		<div class="full-width bg-light">
			<section class="sec-04 container">
				<div class="head-title center-xs wow fadeInDown" data-wow-delay="0.3s">
					<h2 class="h-text">สถานการณ์ COVID-19 ทั่วโลก <small>ข้อมูลล่าสุด 29 ต.ค. 2563</small></h2>
				</div>
				
				<ul class="css-graph-bar mb20-xs wow fadeInUp" data-wow-delay="0.4s">
					<li class="c1" style="width: 12%"><div><span>เสียชีวิต</span> <em>1,261,767</em></div></li>
					<li class="c2" style="width: 65%"><div><span>รักษาหาย</span> <em>35,793,227</em></div></li>
					<li class="c3" style="width: 23%"><div><span>ผู้ติดเชื้อ</span> <em>50,731,151</em></div></li>
				</ul>
				
				<div class="chart-info d-flex end-xs wow bounceInRight" data-wow-delay="0.5s">
					<ul class="d-flex start-xs _chd-mr15">
						<li><i class="point fas fa-circle mr10-sm" style="color: #64C12B"></i> รักษาหาย</li>
						<li><i class="point fas fa-circle mr10-sm" style="color: #F83E3F"></i> เสียชีวิต</li>
						<li><i class="point fas fa-circle mr10-sm" style="color: #F99C19"></i> ผู้ติดเชื้อ</li>
					</ul>
				</div>
				
				<div class="row mt30-xs">
					<div class="_self-cl-xs-12-sm-07">
						<div class="inner">
						<div class="z-map-world wow fadeIn" data-wow-delay="0.3s">
							<!--<img src="di/map-world.png" alt="demo" style="max-width: 100%">-->
							<div id="world-map-gdp" style="width: 100%; height: 360px"></div>
						</div>
						
						<div class="box world-box" style="top: 50%;left: 50%;transform: translate(-50%,-50%)">
							<i class="flag"><img src="di/flag-demo.png" height="100"></i>
							<h3>Canada</h3>
							<p>
								<span><span class="t-gray2">ขาออก : </span>มีความเสี่ยง</span>
								<span><span class="t-gray2">ขาเข้า : </span>เข้าได้กักตัว14วัน</span>
							</p>
							<ul class="d-flex between-xs">
								<li><div class="t-gray2">ติดเชื้อ</div> <em class="t-orange">263,667</em></li>
								<li><div class="t-gray2">รักษาหาย</div> <em class="t-green2">234,543</em></li>
								<li><div class="t-gray2">เสียชีวิต</div> <em class="t-red2">10,909</em></li>
							</ul>
							<a class="close" href="javascript:;" title="Close" onClick="$(this).parent().fadeOut();"></a>
						</div>
						</div>
					</div>
					<div class="_self-cl-xs-12-sm-05 wow fadeIn" data-wow-delay="0.4s">
						<h3 class="h-text small mb15-xs">อันดับประเทศที่ติดไวรัสโควิด-19</h3>
						<div class="inner">
							<div class="head tb-rank">
								<div class="c1">&nbsp;</div>
								<div class="c2">ประเทศ</div>
								<div class="c3">ติดเชื้อ</div>
								<div class="c4">รักษาหาย</div>
								<div class="c5">เสียชีวิต</div>
							</div>
							<div id="rank-covid19" class="content">
								<ol class="list-rank">
									<? for($i=1;$i<=50;$i++){ ?>
									<li>
										<div class="c1"><? echo $i; ?></div>
										<div class="c2"><? if($i%2==0){?>United States<? } else { ?>United Kingdom<? } ?></div>
										<div class="c3">9,870,018</div>
										<div class="c4">3,851,465</div>
										<div class="c5">126,121</div>
									</li>
									<? } ?>
								</ol>
						</div>
						<div class="tb-fixed tb-rank">
								<div class="c1">147</div>
								<div class="c2">Thailand</div>
								<div class="c3">3,840</div>
								<div class="c4">3,661</div>
								<div class="c5">60</div>
						</div>
					</div>
				</div>
				
			</section>
		</div>
		
		<div class="full-width bg-dark">
			<section class="sec-related container">
				<div class="head-title border0 center-xs wow fadeInDown" data-wow-delay="0.3s">
					<h2 class="h-text">ข่าวล่าสุด</h2>
				</div>

				<div class="thm-news row _chd-cl-xs-12-sm-03">
					<article class=" wow fadeInUp" data-wow-delay="0.4s">
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail1.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article class=" wow fadeInUp" data-wow-delay="0.5s">
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail2.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article class=" wow fadeInUp" data-wow-delay="0.6s">
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail3.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article class=" wow fadeInUp" data-wow-delay="0.7s">
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail4.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				</div>
				
				<div class="ctrl-btn d-flex center-xs mt20-xs mb20-xs wow fadeIn" data-wow-delay="0.5s">
					<a class="ui-btn-border-wh btn-more" href="news.php" title="Load more">ดูทั้งหมด <i class="fa-angle-right"></i></a>
				</div>


			</section>
		</div>


		</div>
  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php include("incs/lightbox.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- custom scrollbar plugin -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>
<script src="js/jquery-jvectormap-2.0.5.min.js"></script>
<script src="js/jquery-jvectormap-th-mill.js"></script>
<script src="js/jquery-jvectormap-world-mill.js"></script>
<script src="js/gdp-data.js"></script>

<script>
	//donut
	$(function () {
		$('.knob').knob({});
		$(".knob").parent("div").css("position", "absolute");     
	});
	
	//line
	var ctx = document.getElementById('myChart');
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
				labels: [ // Date Objects
					'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
				],
				datasets: [{
					//label: '',
					backgroundColor: '#F99C19',
					borderColor: '#F99C19',
					fill: false,
					data: [
						110, 120, 130, 140, 50, 160, 170, 80, 90, 100, 80, 90
					],
				}, {
					//label: 'My Second dataset',
					backgroundColor: '#FFD914',
					borderColor: '#FFD914',
					fill: false,
					data: [
						160, 70, 180, 90, 100, 110, 210, 130, 140, 50, 140, 50
					],
				}, {
					//label: 'Dataset with point data',
					backgroundColor: '#64C12B',
					borderColor: '#64C12B',
					fill: false,
					data: [
						160, 170, 110, 120, 130, 140, 150, 180, 90, 100, 90, 100
					],
				}, {
					//label: 'Dataset with point data',
					backgroundColor: '#F83E3F',
					borderColor: '#F83E3F',
					fill: false,
					data: [
						70, 10, 20, 30, 40, 50, 70, 90, 60, 10, 12, 10
					],
				}]
			},
			options: {
				legend: {
					display: false
				},
				tooltips: {
					callbacks: {
					   label: function(tooltipItem) {
							  return tooltipItem.yLabel;
					   }
					}
				}
			}
		/*data: {
			labels: ['รักษาหาย', 'อยู่ในโรงพยาบาล', 'เสียชีวิต'],
			datasets: [{

				data: [20, 10],

			}]
		},
		options: {
        scales: {
            yAxes: [{
                stacked: true
            }]
        }*/
 
	});


	//scorllbar
	(function($){
		$(window).on("load",function(){
			$("#rank-covid19").mCustomScrollbar({theme:"dark-3"});

		});
	})(jQuery);
	
	// jvectormap data
$(function(){
	
	$('#world-map-gdp').vectorMap({
	  map: 'world_mill',
	  backgroundColor  : 'transparent',
	  series: {
		regions: [{
		  values: gdpData,
		  scale: ['#902B2B', '#DC9494'],
		  normalizeFunction: 'polynomial'
		}]
	  },
	  onRegionTipShow: function(e, el, code){
		el.html(el.html()+' (GDP - '+gdpData[code]+')');
	  }
	});
	
  var visitorsData = {"TH-12":"905"};
	  // Thailand map by jvectormap
	  $('#thailand-map').vectorMap({
		map              : 'th_mill',
		backgroundColor  : 'transparent',
		regionStyle      : {
		  initial: {
			fill            : '#75b3ff',
			'fill-opacity'  : 1,
			stroke          : 'none',
			'stroke-width'  : 0,
			'stroke-opacity': 1
		  },
		  selected: {
			fill            : '#75b3ff',
			stroke: 'black',
			'stroke-width'  : 4
			}
		},
		series           : {
		  regions: [
			{
			  values           : visitorsData,
			  scale            : ['#75b3ff', '#ebf4f9'],
			  normalizeFunction: 'polynomial'
			}
		  ]
		},
		onRegionTipShow: function(e, el, code){
			if (typeof visitorsData[code] != 'undefined')
				el.html(el.html()+': usage time '+visitorsData[code]+' mins');
		}
	  });
	  var mapObject = $('#thailand-map').vectorMap('get', 'mapObject');
	  $('.showprovince').mouseover(function(){
		var rel = $(this).attr('rel');
		mapObject.setSelectedRegions('TH-'+rel);
	  });
	  $('.showprovince').mouseout(function(){
		var rel = $(this).attr('rel');
		var obj = new Object();
		obj['TH-'+rel] = false;
		mapObject.setSelectedRegions(obj);
	  });
  });
	
</script>
<!-- /js -->

</body>
</html>
