<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<script type="text/javascript">
//<![CDATA[
document.write('<link href="cs/animate.css" rel="stylesheet" type="text/css">');
//]]>
</script>

<!-- /Top Head -->

<body>
<script>
  //<![CDATA[
  $(document).ready(function(){
	  $('#navigation>ul>li:nth-child(6)>a').addClass('selected');
  });
  //]]>
</script>
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-category">


   <div id="toc">
		<div class="sec-detail">
		<header class="hgroup container">
			<h1>กทม. เปิดเว็บไซต์ BKK covid-19 ประเมินผู้ติดเชื้อ <br>พร้อมจับมือเพจหมอแล็บแพนด้าตรวจ covid-19 ถึงบ้าน</h1>
			<div class="tool-bar d-flex between-xs bottom-xs">
				<span class="date"><i class="fa-calendar-o"></i>  20 พ.ย. 2563 07:48 น</span>
				<div class="share">
					<span><i class="fa-share-alt"></i></span>
					<div class="list">
					<a href="#" title="facebook"><i class="ic-sh-fb"></i></a>
					<a href="#" title="facebook"><i class="ic-sh-line"></i></a>
					<a href="#" title="facebook"><i class="ic-sh-tw"></i></a>
					</div>
				</div>
				
			</div>
		</header>
		<figure class="cover container"><a href="di/cover-detail2.jpg" data-fancybox="gcover"><img src="di/cover-detail2.jpg"></a></figure>
		<div class="container">
			<article class="reader">
				
				<div class="read-body editor">
					<p>นายสุริยะ จึงรุ่งเรืองกิจ รมว.อุตสาหกรรม เปิดเผยว่า วิกฤติโควิด-19 ได้ส่งผลกระทบต่อพฤติกรรมผู้บริโภคที่เปลี่ยนแปลงสู่ชีวิตวิถีใหม่ (New Normal) ที่เน้นสินค้าจำเป็นมากขึ้น ล่าสุดผลศึกษาของสำนักงานเศรษฐกิจอุตสาหกรรม (สศอ.) ได้ประมาณการขยายตัวของอุตสาหกรรมเด่นที่จะ ขยายตัวต่อเนื่องปี 64 มาจากความต้องการในการรักษาโรคและความกังวลโควิด-19 ได้แก่ อุตสาหกรรมเภสัชภัณฑ์และอาหาร คาดว่าตลอดทั้งปีนี้ จะขยายตัว 17.5% เทียบกับปีที่ผ่านมา เช่นเดียวกับอุตสาหกรรมถุงมือยาง ที่คาดว่าปีนี้จะขยายตัว 23.2% มาจากความต้องการใช้ทาง การแพทย์ที่เพิ่มขึ้นทั้งในและต่างประเทศ โดยเฉพาะในสหรัฐฯ อังกฤษ และญี่ปุ่น
</p>
					<figure>
						<a href="di/cover-detail2-1.jpg" data-fancybox="g1"><img src="di/cover-detail2-1.jpg" alt="news"></a>
					</figure>
					<p>ขณะที่อุตสาหกรรมอาหาร (ไม่รวมน้ำตาล) คาดว่าจะขยายตัวเพิ่มขึ้น 1.2% เมื่อเทียบกับปีที่ผ่านมา ได้อานิสงส์จากการสำรองสินค้ าทั้งตลาดในประเทศและส่งออก นอกจากนี้ ยังมีอุตสาหกรรมเครื่องใช้ไฟฟ้าชนิดที่ใช้ในครัวเรือน คาดว่าจะขยายตัว 10% เทียบกับปีที่ผ่านมา จากความต้องการใช้สินค้าประเภทตู้เย็น เตาอบไมโครเวฟ และกระติกน้ำร้อน เนื่องจากผู้บริโภคต้องการสำรองอาหารสดไว้ที่บ้านเพิ่มขึ้น และผู้ประกอบการลดราคาสินค้า ส่วนเครื่องซักผ้ามีการส่งออกไปตลาดอาเซียนเพิ่มขึ้น ด้านอุตสาหกรรมหลักๆ เช่น อุตสาหกรรมยานยนต ์และยางล้อ, ปิโตรเลียม, เหล็กและเหล็กกล้า เริ่มฟื้นตัวตามการฟื้นตัวของการบริโภคภาคเอกชนหรือครัวเรือน และการจ้างงานเริ่มกลับ สู่ภาวะใกล้เคียงปกติแล้ว ประกอบกับเศรษฐกิจประเทศคู่ค้าที่เริ่มกลับมาดีขึ้น รวมถึงมาตรการรัฐที่ฟื้นฟูและกระตุ้นเศรษฐกิจภายในประเทศ</p>
					<figure>
						<a href="di/cover-detail2-2.jpg" data-fancybox="g1"><img src="di/cover-detail2-2.jpg" alt="news"></a>
					</figure>
					<p>“อุตสาหกรรมอาหารและเภสัชภัณฑ์จะเป็นอุตสาหกรรมดาวรุ่งในปี 64 เนื่องจากตอบสนองต่อพฤติกรรมการบริโภคที่เปลี่ยนแปลงอย่างรวดเร็วได้ แต่ผู้ประกอบการจำเป็นต้องปรับตัว โดยเฉพาะการใช้เทคโนโลยี เพื่อลดต้นทุนและเพิ่มขีดความสามารถในการแข่งขัน ปรับรูปแบบการจัดการ ห่วงโซ่การผลิต มุ่งใช้ทรัพยากรให้คุ้มค่า ลดการปล่อยมลภาวะและสร้างเศรษฐกิจสีเขียว ตอบสนองต่อความต้องการสินค้าที่เป็นมิตรกับสิ่งแวดล้อม เพื่อรักษาฐานการผลิตที่สำคัญของโลกและพัฒนาสินค้าที่มีเทคโนโลยีและนวัตกรรมให้แข่งขันในตลาดโลกได้อย่างยั่งยืน”.</p>
					<!--
					
					<ul class="ft-gallery row _chd-cl-xs-06-mb20">
						<li><a href="di/banner/thm-news-01.png" data-fancybox="g1"><img src="di/banner/thm-news-01.png" alt="news"></a></li>
						<li><a href="di/banner/thm-news-02.png" data-fancybox="g1"><img src="di/banner/thm-news-02.png" alt="news"></a></li>
						<li><a href="di/banner/thm-news-03.png" data-fancybox="g1"><img src="di/banner/thm-news-03.png" alt="news"></a></li>
						<li><a href="di/banner/thm-news-04.png" data-fancybox="g1"><img src="di/banner/thm-news-04.png" alt="news"></a></li>
					</ul>-->
				</div>
			</article>

			<section class="sec-related wow fadeIn" data-wow-delay="0.5s">
				<div class="head-title border0 start-xs">
					<h2 class="h-line"><a href="#all">แนะนำบทความที่น่าสนใจ</a></h2>
				</div>

				<div class="thm-news row _chd-cl-xs-12-sm-03">
					<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail1.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail2.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail3.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				<article>
					<a class="in" href="detail.php" title="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						<figure>
							<img src="di/thumbnail4.png" alt="เปิด 5 อาชีพเสี่ยงติดโควิด-19">
						</figure>
						<div class="detail">
							<h3>เปิด 5 อาชีพเสี่ยงติดโควิด-19</h3>
							<div class="tools">
								<i class="fa-calendar-o"></i> <span class="date">20 พ.ย. 2563</span> <span class="time">07:48 น</span>
							</div>
							<p>หากพิจารณาถึงอาชีพเสี่ยงโดยแบ่งตามระยะระบาดของโรคที่กำหนดโดยรัฐบาลไทย จะสามารถจำแนกอาชีพที่สัมพันธ์ได้กับ...</p>
							
						</div>
					</a>
				</article>
				</div>


			</section>
		</div>
		
		
		
		

		</div>
  </div>
</div>
<!-- footer -->
<?php include("incs/footer.html") ?>
<?php /*?><?php include("incs/lightbox.html") ?><?php */?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->

</body>
</html>
